package dv.spring.kotlin.entity

import javax.persistence.*

@Entity
data class SelectedProduct(var quantity: Int?=null){
    @Id
    @GeneratedValue
    var id:Long? = null
    @ManyToOne
    var product:Product? = null

    constructor( quantity: Int, product: Product): this(quantity){ this.product = product}
}