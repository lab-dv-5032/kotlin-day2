package dv.spring.kotlin.entity.dto

import dv.spring.kotlin.entity.UserStatus

data class CustomerDto (var name:String?=null,
                        var email:String?=null,
                        var userStatus: UserStatus?=null,
                        var addr: AddressDto?=null,
                        var id:Long? = null) {
}