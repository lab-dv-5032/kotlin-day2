package dv.spring.kotlin.entity.dto

import dv.spring.kotlin.entity.SelectedProduct
import dv.spring.kotlin.entity.ShoppingCartStatus

data class ShoppingCartDto(var status:ShoppingCartStatus?=null,
                           var selectedProduct: List<SelectedProductDto>?= mutableListOf<SelectedProductDto>() ) {

}