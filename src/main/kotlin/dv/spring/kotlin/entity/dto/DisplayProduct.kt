package dv.spring.kotlin.entity.dto

class DisplayProduct (var name:String? = null,
                      var description: String? = null,
                      var quantity: Int? = null) {
}