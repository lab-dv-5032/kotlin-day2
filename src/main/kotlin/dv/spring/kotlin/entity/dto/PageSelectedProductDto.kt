package dv.spring.kotlin.entity.dto

class PageSelectedProductDto(var totalElements: Long? = null,
                             var totalPages: Int? = null,
                             var selectedProduct: List<SelectedProductDto> = mutableListOf()) {
}