package dv.spring.kotlin.entity.dto

class PageProductDto(var totalPages: Int? = null,
                     var totalElements: Long? = null,
                     var products: List<ProductDto> = mutableListOf()) {
}