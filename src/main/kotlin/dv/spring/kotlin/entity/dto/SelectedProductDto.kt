package dv.spring.kotlin.entity.dto

import dv.spring.kotlin.entity.Product

data class SelectedProductDto(var quantity:Int?=null, var product:ProductDto?=null) {
}