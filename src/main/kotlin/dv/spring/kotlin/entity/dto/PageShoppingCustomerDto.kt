package dv.spring.kotlin.entity.dto

class PageShoppingCustomerDto (var totalPage : Int,
                               var totalElement: Long,
                               var shoppingList: List<ShoppingCustomerDto> ) {
}