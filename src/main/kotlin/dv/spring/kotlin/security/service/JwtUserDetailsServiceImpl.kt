package dv.spring.kotlin.security.service


import dv.spring.kotlin.security.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Primary
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service

@Primary
@Service
class JwtUserDetailsServiceImpl : UserDetailsService {
    @Autowired
    lateinit var userRepository: UserRepository

    override fun loadUserByUsername(username: String?): UserDetails {
        var user = username?.let{userRepository.findByUsername(it)}
        if (user == null){
            throw UsernameNotFoundException(String.format("No user found with username '%s'.", username))
        }
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}