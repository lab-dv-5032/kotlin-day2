package dv.spring.kotlin.security.repository

import dv.spring.kotlin.security.entity.Authority
import dv.spring.kotlin.security.entity.AuthorityName
import org.springframework.data.repository.CrudRepository

interface AuthorityRepository: CrudRepository<Authority, Long> {
    fun findByName(input: AuthorityName) : Authority
}