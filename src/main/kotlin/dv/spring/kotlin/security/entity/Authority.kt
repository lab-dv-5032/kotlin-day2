package dv.spring.kotlin.security.entity

import javax.persistence.GeneratedValue
import javax.persistence.*

@Entity
data class Authority(var name: AuthorityName? = null) {
    @Id
    @GeneratedValue
    var id:Long? = null
    @ManyToMany(mappedBy = "authorities")
    var jwtUser = mutableListOf<JwtUser>()

}