package dv.spring.kotlin.dao

import dv.spring.kotlin.entity.Customer

interface CustomerDao {
    fun getCustomers(): List<Customer>
    fun getCustomerByName(name:String): Customer?
    fun getCustomerByPartialName(name: String): List<Customer>
    fun getCustomerByPartialNameAndEmail(name: String, email:String): List<Customer>
    fun getCustomerByProvince(province: String): List<Customer>
    fun getCustomerByStatus(status: String): List<Customer>
    fun save(customer: Customer):Customer
    fun findById(id: Long): Customer?
}