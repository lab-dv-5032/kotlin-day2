package dv.spring.kotlin.dao

import dv.spring.kotlin.entity.Customer
import dv.spring.kotlin.entity.UserStatus
import dv.spring.kotlin.repository.CustomerRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository
import java.lang.IllegalArgumentException

@Repository
class CustomerDaoDBImpl : CustomerDao {


    @Autowired
    lateinit var customerRepository: CustomerRepository

    override fun getCustomers(): List<Customer> {
//        return customerRepository.findAll().filterIsInstance(Customer::class.java)
        return customerRepository.findByIsDeletedIsFalse()
    }

    override fun getCustomerByName(name: String): Customer {
        return customerRepository.findByName(name)
    }

    override fun getCustomerByPartialName(name: String): List<Customer> {
        return customerRepository.findByNameEndingWithIgnoreCase(name)
    }

    override fun getCustomerByPartialNameAndEmail(name: String, email:String): List<Customer> {
        return customerRepository.findByNameContainingIgnoreCaseOrEmailContainingIgnoreCase(name,name)
    }

    override fun getCustomerByProvince(province: String): List<Customer> {
        return customerRepository.findByDefaultAddress_ProvinceContainingIgnoreCase(province)
    }

    override fun getCustomerByStatus(status: String): List<Customer> {
        return try {
            customerRepository.findByUserStatus(UserStatus.valueOf(status.toUpperCase()))
        } catch (e:IllegalArgumentException){
            emptyList()
        }
    }

    override fun save(customer: Customer): Customer {
        return customerRepository.save(customer)
    }

    override fun findById(id: Long): Customer? {
        return customerRepository.findById(id).orElse(null)
    }

}