package dv.spring.kotlin.dao

import dv.spring.kotlin.entity.SelectedProduct
import dv.spring.kotlin.repository.SelectedProductRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest

import org.springframework.stereotype.Repository

@Repository
class SelectedProductDaoDBImpl : SelectedProductDao {
    @Autowired
    lateinit var selectedProductRepository: SelectedProductRepository
    override fun findProductByName(name: String, page:Int, pageSize:Int): Page<SelectedProduct> {
        return selectedProductRepository.findByProduct_NameIgnoreCase(name, PageRequest.of(page,pageSize))
    }
}