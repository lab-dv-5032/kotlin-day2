package dv.spring.kotlin.dao

import dv.spring.kotlin.entity.Address
import dv.spring.kotlin.repository.AddressRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository

@Repository
class AddressDaoImpl : AddressDao {

    @Autowired
    lateinit var addressRepository : AddressRepository
    override fun save(address: Address): Address {
        return addressRepository.save(address)
    }
    override fun findById(addrId: Long): Address? {
        return addressRepository.findById(addrId).orElse(null)
    }
}