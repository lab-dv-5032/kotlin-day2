package dv.spring.kotlin.dao

import dv.spring.kotlin.entity.ShoppingCart
import org.springframework.data.domain.Page

interface ShoppingCartDao {
    fun getShoppingCarts(page:Int, pageSize:Int) : Page<ShoppingCart>
    fun findByProductName(name: String, page: Int, pageSize: Int): Page<ShoppingCart>
    fun findByProductName(name: String): List<ShoppingCart>
}