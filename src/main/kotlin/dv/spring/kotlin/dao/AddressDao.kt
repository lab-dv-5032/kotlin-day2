package dv.spring.kotlin.dao

import dv.spring.kotlin.entity.Address

interface AddressDao {
    fun save(address: Address):Address
    fun findById(addrId: Long): Address?
}
