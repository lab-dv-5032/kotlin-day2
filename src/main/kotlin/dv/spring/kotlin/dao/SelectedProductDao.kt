package dv.spring.kotlin.dao

import dv.spring.kotlin.entity.SelectedProduct
import org.springframework.data.domain.Page

interface SelectedProductDao {
    fun findProductByName(name: String, page:Int, pageSize:Int): Page<SelectedProduct>

}
