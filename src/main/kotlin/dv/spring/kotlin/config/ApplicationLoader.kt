package dv.spring.kotlin.config

import dv.spring.kotlin.entity.*
import dv.spring.kotlin.repository.*
import dv.spring.kotlin.security.repository.UserRepository
import dv.spring.kotlin.security.entity.Authority
import dv.spring.kotlin.security.entity.AuthorityName
import dv.spring.kotlin.security.entity.JwtUser
import dv.spring.kotlin.security.repository.AuthorityRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Component
import javax.transaction.Transactional

@Component
class ApplicationLoader: ApplicationRunner{
    @Autowired
    lateinit var manufacturerRepository: ManufacturerRepository
    @Autowired
    lateinit var  productRepository: ProductRepository
    @Autowired
    lateinit var  customerRepository: CustomerRepository
    @Autowired
    lateinit var  shoppingCartRepository: ShoppingCartRepository
    @Autowired
    lateinit var addressRepository: AddressRepository
    @Autowired
    lateinit var selectedProductRepository: SelectedProductRepository
    @Autowired
    lateinit var dataLoader: DataLoader
    @Autowired
    lateinit var authorityRepository: AuthorityRepository
    @Autowired
    lateinit var userRepository: UserRepository

    @Transactional
    fun loadUsernameAndPassword(){
        val auth1 = Authority(name = AuthorityName.ROLE_ADMIN)
        val auth2 = Authority(name = AuthorityName.ROLE_CUSTOMER)
        val auth3 = Authority(name = AuthorityName.ROLE_GENERAL)

        authorityRepository.save(auth1)
        authorityRepository.save(auth2)
        authorityRepository.save(auth3)

        val encoder = BCryptPasswordEncoder()
        val cust1 = Customer(name = "สมชาติ", email = "a@b.com")
        val custJwt = JwtUser(
                username = "customer",
                password = encoder.encode("password"),
                email = cust1.email,
                enabled = true,
                firstname = cust1.name,
                lastname = "unknow"
        )
        customerRepository.save(cust1)
        userRepository.save(custJwt)
        cust1.jwtUser = custJwt
        custJwt.user = cust1
        custJwt.authorities.add(auth2)
        custJwt.authorities.add(auth3)
    }
    @Transactional
    override fun run(args: ApplicationArguments?) {

        var manu1 = manufacturerRepository.save(Manufacturer("CAMT","0000000"))
        var manu2 = manufacturerRepository.save(Manufacturer("Apple","053123456"))
        var manu3 = manufacturerRepository.save(Manufacturer("SAMSUNG","555666777888"))

        var product1 = productRepository.save(
                Product("CAMT",
                        "the best College in CMU",
                        0.0,
                        1,
                        "http://www.camt.cmu.ac.th/th/images/logo.jpg"))
        var product2 = productRepository.save(
                Product("iPhone",
                        "It's a phone",
                        28000.0,
                        20,
                        "https://www.jaymartstore.com/Products/iPhone-X-64GB-Space-Grey--1140900010552--4724"))
        var product3 = productRepository.save(
                Product("Note 9",
                    "Other Iphone",
                    28001.0,
                    10,
                    "http://dynamic-cdn.eggdigital.com/e56zBiUt1.jpg"))
        var product4 = productRepository.save(
                Product("Prayuth",
                        "The best PM ever",
                        1.0,
                        1,
                        "https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Prayut_Chan-o-cha_%28cropped%29_2016.jpg/200px-Prayut_Chan-o-cha_%28cropped%29_2016.jpg"))

        manu1.products.add(product1)
        manu1.products.add(product4)
        manu2.products.add(product2)
        manu3.products.add(product3)

        product1.manufacturer = manu1
        product4.manufacturer = manu1
        product2.manufacturer = manu2
        product3.manufacturer = manu3

        //Customer Obj
        var customer1 = customerRepository.save(
                Customer("Lung",
                        "pm@go.th",
                        UserStatus.ACTIVE)
        )
        var customer2 = customerRepository.save(
                Customer("ชัชชาติ",
                        "chut@taopoon.com",
                        UserStatus.ACTIVE)
        )
        var customer3 = customerRepository.save(
                Customer("ธนาธร",
                        "Thanathorn@life.com",
                        UserStatus.PENDING)
        )

        var address1 = addressRepository.save(Address("ถนนอนุสาวรีย์ประชาธิปไตย",
                "ดินสอ","ดุสิต","กรุงเทพ","10123"))

        var address2 = addressRepository.save(Address("239 มหาวิทยาลัยเชียงใหม่",
                "สุเทพ","เมือง","เชียงใหม่","50200"))

        var address3 = addressRepository.save(Address("ซักที่บนโลก",
                "ต.สุขสันต์", "อ.ในเมือง", "จ.ขอนแก่น", "12457"))

        customer1.defaultAddress=address1
        customer2.defaultAddress=address2
        customer3.defaultAddress=address3


        //shopping cart obj
        var cart1 = shoppingCartRepository.save(ShoppingCart(ShoppingCartStatus.SENT))
        var cart2 = shoppingCartRepository.save(ShoppingCart())

        cart1.customer = customer1
        cart2.customer = customer2

        //เพิ่ม constructor ใน SelectedProduct
        var selectedProduct1 = selectedProductRepository.save(SelectedProduct(4, product2))
        var selectedProduct2 = selectedProductRepository.save(SelectedProduct(1, product4))
        var selectedProduct3 = selectedProductRepository.save(SelectedProduct(1, product4))
        var selectedProduct4 = selectedProductRepository.save(SelectedProduct(1, product1))
        var selectedProduct5 = selectedProductRepository.save(SelectedProduct(2, product3))

        cart1.selectedProducts.add(selectedProduct1)
        cart1.selectedProducts.add(selectedProduct2)

        cart2.selectedProducts.add(selectedProduct3)
        cart2.selectedProducts.add(selectedProduct4)
        cart2.selectedProducts.add(selectedProduct5)

        dataLoader.loadData()
        loadUsernameAndPassword()



    }
}