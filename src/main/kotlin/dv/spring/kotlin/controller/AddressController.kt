package dv.spring.kotlin.controller

import dv.spring.kotlin.entity.Address
import dv.spring.kotlin.entity.dto.AddressDto
import dv.spring.kotlin.service.AddressService
import dv.spring.kotlin.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class AddressController {

    @Autowired
    lateinit var addressSevice : AddressService

    @PostMapping("/address")
    fun addAddress(@RequestBody address: AddressDto) : ResponseEntity<Any> {
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapAddress(addressSevice.save(address)))
    }

    @PutMapping("/address/{addrId}")
    fun updateAddress(@PathVariable("addrId") id:Long?,
                      @RequestBody addr: AddressDto): ResponseEntity<Any> {
        addr.id = id
        return ResponseEntity.ok(
                MapperUtil.INSTANCE.mapAddress(addressSevice.save(addr))
        )
    }
}