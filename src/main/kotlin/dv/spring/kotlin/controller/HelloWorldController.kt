package dv.spring.kotlin.controller

import dv.spring.kotlin.entity.Person
import dv.spring.kotlin.entity.myPerson
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class HelloWorldController{

    @GetMapping("/helloWorld")
    fun getHelloWorld(): String{
        return "HelloWorld"
    }

    @GetMapping("/person")
    fun getPerson(): ResponseEntity<Any>{
        val person = Person("somchai", "somrak", 15)
        return ResponseEntity.ok(person)
    }

    @GetMapping("/myPerson")
    fun getMyPerson(): ResponseEntity<Any>{
        val person = Person("supisara", "Titi", 21)
        return ResponseEntity.ok(person)
    }

    @GetMapping("/persons")
    fun getPersons(): ResponseEntity<Any>{
        val person1 = Person("somchai","somrak",15)
        val person2 = Person("Prayut","Chan",62)
        val person3 = Person("Lung","Pom",65)
        val persons = listOf<Person>(person1, person2, person3)
        return ResponseEntity.ok(persons)
    }

    @GetMapping("/myPersons")
    fun getMyPersons(): ResponseEntity<Any>{
        val myPerson1 = myPerson("Osora", "Tsubasa", "Nachansu", 10)
        val myPerson2 = myPerson("Hyuoka", "Kojiro", "Meiwa", 9)
        val myPersons = listOf<myPerson>(myPerson1, myPerson2) // listOf<>() แก้ไม่ได้ ขยาย/ลด ขนาดไม่ได้แล้วต้องใช้ตัวอื่น
        return ResponseEntity.ok(myPersons)
    }

    @GetMapping("/params")
    fun getParams(@RequestParam("name")name:String, @RequestParam("surname")surname:String): ResponseEntity<Any>{
        return ResponseEntity.ok("$name $surname")
    }

    @GetMapping("/params/{name}/{surname}/{age}")
    fun getPathParam(@PathVariable("name") name:String, @PathVariable("surname") surname:String, @PathVariable("age") age:Int)
        : ResponseEntity<Any> {
        val person = Person(name,surname,age)
        return ResponseEntity.ok(person)
    }

    @PostMapping("/echo")
    fun echo(@RequestBody person:Person) : ResponseEntity<Any>{
        return ResponseEntity.ok(person)
    }
}
