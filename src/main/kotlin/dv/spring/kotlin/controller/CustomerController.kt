package dv.spring.kotlin.controller

import dv.spring.kotlin.entity.dto.CustomerDto
import dv.spring.kotlin.service.CustomerService
import dv.spring.kotlin.service.ProductService
import dv.spring.kotlin.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.xml.ws.Response

@RestController
class CustomerController {
    @Autowired
    lateinit var customerService : CustomerService

    @GetMapping("/customer")
    fun getCustomer() : ResponseEntity<Any> {
        val customers = customerService.getCustomer()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapCustomer(customers))
    }

    @GetMapping("/customer/query")
    fun getCustomerByName(@RequestParam("name")name:String):ResponseEntity<Any>{
        var output = MapperUtil.INSTANCE.mapCustomer(customerService.getCustomerByName(name))
        output?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @GetMapping("customers/partialQuery")
    fun getCustomerPartial(@RequestParam("name")name:String,
                           @RequestParam(value="email",required = false)email:String?):ResponseEntity<Any>{
        val output:List<CustomerDto> = MapperUtil.INSTANCE.mapCustomer(
                customerService.getCustomerByPartialNameAndEmail(name,name))
        return ResponseEntity.ok(output)
    }

    @GetMapping("customers/province")
    fun getCustomerByProvince(@RequestParam("province")province:String):ResponseEntity<Any>{
        val output = MapperUtil.INSTANCE.mapCustomer(customerService.getCustomerByProvince(province))
        return ResponseEntity.ok(output)
    }

    @GetMapping("customer/status")
    fun getCustomerByStatus(@RequestParam("status") status: String): ResponseEntity<Any> {
        return ResponseEntity.ok(customerService.getCustomerByStatus(status))
    }
//    task no.5
//    @GetMapping("/customer/product")
//    fun getCustomerByBoughtProduct(@RequestParam("name") name:String): ResponseEntity<Any> {
//        return ResponseEntity.ok(customerService.findByBoughtProduct(name))
//    }

    @PostMapping("/customer")
    fun addCustomer(@RequestBody customerDto: CustomerDto): ResponseEntity<Any> {
        val output = customerService.save(MapperUtil.INSTANCE.mapCustomer(customerDto))
        val outputDto = MapperUtil.INSTANCE.mapCustomer(output)
        outputDto?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @PostMapping("/customer/address/{addrId}")
    fun addCustomer(@PathVariable addrId : Long,
                    @RequestBody customerDto: CustomerDto) : ResponseEntity<Any>{
        val output = customerService.save(addrId,MapperUtil.INSTANCE.mapCustomer(customerDto))
        val outputDto = MapperUtil.INSTANCE.mapCustomer(output)
        outputDto?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build()
    }

    @DeleteMapping("/customer/{id}")
    fun deleteCustomer(@PathVariable("id") id:Long):ResponseEntity<Any>{
        val customer = customerService.remove(id)
        val outputCustomer = MapperUtil.INSTANCE.mapCustomer(customer)
        outputCustomer?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("the customer id is not found")
    }
}