package dv.spring.kotlin.controller

import dv.spring.kotlin.entity.dto.PageShoppingCustomerDto
import dv.spring.kotlin.service.ShoppingCartService
import dv.spring.kotlin.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class ShoppingCartController {
    @Autowired
    lateinit var shoppingCartService: ShoppingCartService

//    @GetMapping("/shoppingcart")
//    fun getShoppingCarts() : ResponseEntity<Any> {
//        val shoppingCarts = shoppingCartService.getShoppingCarts()
//        return ResponseEntity.ok(MapperUtil.INSTANCE.mapShoppingCart(shoppingCarts))
//    }

    @GetMapping("/shoppingcart/productName")
    fun getShoppingCartByProductName(
            @RequestParam("name")name:String,
            @RequestParam("page") page:Int,
            @RequestParam("pageSize") pageSize:Int) : ResponseEntity<Any> {
        val shoppingCarts = shoppingCartService.getShoppingCartByProductName(name, page, pageSize)
        return ResponseEntity.ok(PageShoppingCustomerDto(
                totalPage = shoppingCarts.totalPages,
                totalElement = shoppingCarts.totalElements,
                shoppingList = MapperUtil.INSTANCE.mapShoppingCustomerDao(shoppingCarts.content)
        ))
    }

    @GetMapping("/allShoppingCart")
    fun getAllShoppingCart(
            @RequestParam("page")page : Int,
            @RequestParam("pageSize") pageSize: Int
    ) : ResponseEntity<Any> {
        val output = shoppingCartService.getShoppingCarts(page, pageSize)
        return ResponseEntity.ok(PageShoppingCustomerDto(
                totalPage = output.totalPages,
                totalElement = output.totalElements,
                shoppingList = MapperUtil.INSTANCE.mapShoppingCustomerDao(output.content)

        ))
    }

//    @PostMapping("/shoppingcart")
//    fun addShopiingCart(@RequestBody plainProduct: PlainProduct): ResponseEntity<Any>{
//        val output = shoppingCartService.save()
//    }
}