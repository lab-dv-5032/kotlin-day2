package dv.spring.kotlin.service

import dv.spring.kotlin.entity.ShoppingCart
import org.springframework.data.domain.Page

interface ShoppingCartService {
    fun getShoppingCarts(page: Int, pageSize:Int) : Page<ShoppingCart>
    fun getShoppingCartByProductName(name: String, page: Int, pageSize: Int): Page<ShoppingCart>
}