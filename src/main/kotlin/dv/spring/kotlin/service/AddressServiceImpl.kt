package dv.spring.kotlin.service

import dv.spring.kotlin.dao.AddressDao
import dv.spring.kotlin.entity.Address
import dv.spring.kotlin.entity.dto.AddressDto
import dv.spring.kotlin.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class AddressServiceImpl : AddressService {
    @Autowired
    lateinit var addressDao : AddressDao
    override fun save(addr: AddressDto): Address {
        val address = MapperUtil.INSTANCE.mapAddress(addr)
        return addressDao.save(address)
    }
}