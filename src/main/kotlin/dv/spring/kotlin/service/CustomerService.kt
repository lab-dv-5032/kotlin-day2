package dv.spring.kotlin.service

import dv.spring.kotlin.entity.Customer

interface CustomerService {
    fun getCustomer() : List<Customer>
    fun getCustomerByName(name:String): Customer?
    fun getCustomerByPartialName(name: String): List<Customer>
    fun getCustomerByPartialNameAndEmail(name: String, email:String): List<Customer>
    fun getCustomerByProvince(province: String): List<Customer>
    fun getCustomerByStatus(status: String): List<Customer>
//    fun findByBoughtProduct(name: String): List<Customer>
    fun save(customer: Customer):Customer
    fun save(addrId: Long, customer: Customer):Customer
    fun remove(id:Long):Customer?

}