package dv.spring.kotlin.service

import dv.spring.kotlin.dao.AddressDao
import dv.spring.kotlin.dao.CustomerDao
import dv.spring.kotlin.dao.ShoppingCartDao
import dv.spring.kotlin.entity.Customer
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import javax.transaction.Transactional

@Service
class CustomerServiceImpl : CustomerService {

    override fun getCustomerByPartialNameAndEmail(name: String, email: String): List<Customer> {
        return customerDao.getCustomerByPartialNameAndEmail(name,email)
    }

    @Autowired
    lateinit var customerDao: CustomerDao

    @Autowired
    lateinit var shoppingCartDao: ShoppingCartDao

    @Autowired
    lateinit var addressDao: AddressDao

    override fun getCustomer(): List<Customer> {
        return customerDao.getCustomers()
    }

    override fun getCustomerByName(name: String): Customer?
      = customerDao.getCustomerByName(name)

    override fun getCustomerByPartialName(name: String): List<Customer> {
        return customerDao.getCustomerByPartialName(name)
    }

    override fun getCustomerByProvince(province: String): List<Customer> {
        return customerDao.getCustomerByProvince(province)
    }

    override fun getCustomerByStatus(status: String): List<Customer> {
        return customerDao.getCustomerByStatus(status)
    }

//    override fun findByBoughtProduct(name: String): List<Customer> {
//        return shoppingCartDao.findByProductName(name)
//                .map { shoppingCart -> shoppingCart.customer }.toSet().toList()
//    }
    @Transactional
    override fun save(customer: Customer): Customer {
        val defaultAddress = customer.defaultAddress?.let { addressDao.save(it) }
        val customer = customerDao.save(customer)
        return customer
    }

    @Transactional
    override fun save(addrId: Long, customer: Customer): Customer {
        val defaultAddress = addressDao.findById(addrId)
        val customer = customerDao.save(customer)
        customer.defaultAddress = defaultAddress
        return customer
    }

    @Transactional
    override fun remove(id: Long): Customer? {
        val customer = customerDao.findById(id)
        customer?.isDeleted = true
        return customer
    }


}