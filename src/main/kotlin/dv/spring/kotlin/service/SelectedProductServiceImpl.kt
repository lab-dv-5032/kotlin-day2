package dv.spring.kotlin.service

import dv.spring.kotlin.dao.SelectedProductDao
import dv.spring.kotlin.entity.SelectedProduct
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service

@Service
class SelectedProductServiceImpl : SelectedProductService {
    @Autowired
    lateinit var selectedProductDao : SelectedProductDao
    override fun findProductByName(name: String, page:Int, pageSize:Int): Page<SelectedProduct> {
        return selectedProductDao.findProductByName(name,page,pageSize)
    }
}