package dv.spring.kotlin.service

import dv.spring.kotlin.dao.ShoppingCartDao
import dv.spring.kotlin.entity.ShoppingCart
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Service

@Service
class ShoppingCartServiceImpl : ShoppingCartService {

    @Autowired
    lateinit var shoppingCartDao: ShoppingCartDao
    override fun getShoppingCarts(page:Int, pageSize:Int): Page<ShoppingCart> {
        return shoppingCartDao.getShoppingCarts(page, pageSize)
    }

    override fun getShoppingCartByProductName(name: String, page: Int, pageSize: Int): Page<ShoppingCart> {
        return shoppingCartDao.findByProductName(name, page, pageSize)
    }

}