package dv.spring.kotlin.util

import dv.spring.kotlin.dao.ProductDao
import dv.spring.kotlin.entity.*
import dv.spring.kotlin.entity.dto.*
import org.mapstruct.InheritInverseConfiguration
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.Mappings
import org.mapstruct.factory.Mappers

@Mapper (componentModel = "spring")
interface MapperUtil{
    companion object {
        val INSTANCE = Mappers.getMapper(MapperUtil::class.java)
    }

    @Mappings(
            Mapping(source="manufacturer", target="manu")
    )

    fun mapProductDto(product: Product?): ProductDto?

    fun mapProductDto(products: List<Product>): List<ProductDto>

    fun mapManufacturer(manu: Manufacturer): Manufacturer


    @Mappings(Mapping(source = "defaultAddress", target = "addr"))
    fun mapCustomer(customer: Customer?):CustomerDto?
    fun mapCustomer(customers: List<Customer>): List<CustomerDto>

    fun mapAddress(addr: Address) : AddressDto

    @Mappings(Mapping(source="selectedProducts",target = "selectedProduct"))
    fun mapShoppingCart(shoppingcart: ShoppingCart): ShoppingCartDto
    fun mapShoppingCart(shoppingcart: List<ShoppingCart>):List<ShoppingCartDto>

    @Mappings(Mapping(source="product",target = "product"))
    fun mapSelectedProductDto(selectedProduct: SelectedProduct): SelectedProductDto
    fun mapSelectedProductDto(selectedProduct: List<SelectedProduct>): List<SelectedProductDto>

//    @Mappings(Mapping(source = "selectedProducts", target = "selectedProduct"))
//    fun mapAnotherShoppingCart(shoppingCartDto: ShoppingCartDto): AnotherShoppingCartDto
//    fun mapAnotherShopiingCart(shoppingCartDto: List<ShoppingCartDto>): List<AnothershoppingCartDto>

    @InheritInverseConfiguration
    fun mapManufacturer(manu: ManufacturerDto) : Manufacturer

    @InheritInverseConfiguration
    fun mapAddress(addr : AddressDto) : Address

    @Mappings(
            Mapping(source = "product.name", target = "name"),
            Mapping(source = "product.description", target = "description"),
            Mapping(source = "quantity", target = "quantity")
    )
    fun mapProductDisplay(selectedProduct: SelectedProduct) : DisplayProduct
    fun mapProductDisplay(selectedProduct: List<SelectedProduct>) : List<DisplayProduct>

    @Mappings(
            Mapping(source = "customer.name", target = "customer"),
            Mapping(source = "customer.defaultAddress", target = "address"),
            Mapping(source = "selectedProducts", target = "products")
    )
    fun mapShoppingCustomerDao(shoppingcart: ShoppingCart) : ShoppingCustomerDto
    fun mapShoppingCustomerDao(shoppingcart: List<ShoppingCart>) : List<ShoppingCustomerDto>

    @InheritInverseConfiguration
    fun mapProductDto(productDto: ProductDto):Product

    @InheritInverseConfiguration
    fun mapCustomer(customerDto: CustomerDto) : Customer

}