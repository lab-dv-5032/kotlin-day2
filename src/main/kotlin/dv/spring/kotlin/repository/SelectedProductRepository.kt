package dv.spring.kotlin.repository

import dv.spring.kotlin.entity.SelectedProduct
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.CrudRepository


interface SelectedProductRepository : CrudRepository<SelectedProduct,Long> {
    fun findByProduct_NameIgnoreCase(name:String, pageable: Pageable) : Page<SelectedProduct>
}