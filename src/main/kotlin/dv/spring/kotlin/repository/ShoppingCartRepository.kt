package dv.spring.kotlin.repository

import dv.spring.kotlin.entity.ShoppingCart
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.repository.CrudRepository


interface ShoppingCartRepository : CrudRepository<ShoppingCart,Long> {
//    fun findBySelectedProducts_Product_NameContainingIgnoreCase(): List<ShoppingCart>
    fun findAll(pageable: Pageable) : Page<ShoppingCart>
    fun findBySelectedProducts_Product_NameContainingIgnoreCase(name: String, pageable: Pageable): Page<ShoppingCart>
    fun findBySelectedProducts_Product_NameContainingIgnoreCase(name: String): List<ShoppingCart>
}