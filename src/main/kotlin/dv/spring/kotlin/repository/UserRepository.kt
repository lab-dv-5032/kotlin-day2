package dv.spring.kotlin.repository

import dv.spring.kotlin.entity.Customer
import dv.spring.kotlin.entity.User
import org.springframework.data.repository.CrudRepository

interface UserRepository:CrudRepository<User,Long> {
    fun findByUsername(username:String) : User

}
